package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static DBManager instance;

    private static final String GET_ALL_USERS = "SELECT * FROM users ORDER BY id";
    private static final String GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ? ORDER BY id";
    private static final String INSERT_USER = "INSERT INTO users VALUES (Default, ?)";
    private static final String DELETE_USER = "DELETE FROM users WHERE id = ? AND login = ?";

    private static final String GET_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
    private static final String GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ? ORDER BY id";
    private static final String INSERT_TEAM = "INSERT INTO teams VALUES (Default, ?)";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ? AND name = ?";
    private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

    private static final String SET_TEAM_FOR_USER = "INSERT INTO users_teams(user_id, team_id) VALUES (?, ?)";
    private static final String GET_USER_TEAM = "SELECT teams.id, teams.name FROM users_teams " +
            "JOIN teams " +
            "ON users_teams.team_id = teams.id " +
            "WHERE users_teams.user_id = ?";

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        try (Statement statement = connect().createStatement()) {
            ResultSet res = statement.executeQuery(GET_ALL_USERS);
            List<User> users = new ArrayList<>();
            while (res.next()) {
                User newUser = User.createUser(res.getString("login"));
                newUser.setId(res.getInt("id"));
                users.add(newUser);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean insertUser(User user) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getLogin());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
                return true;
            } else
                return false;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(DELETE_USER)) {
            for (User user : users) {
                statement.setInt(1, user.getId());
                statement.setString(2, user.getLogin());
                statement.executeUpdate();
            }
            return true;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public User getUser(String login) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(GET_USER_BY_LOGIN)) {
            statement.setString(1, login);
            ResultSet res = statement.executeQuery();
            User user = User.createUser(login);
            if (res.next()) {
                user.setId(res.getInt("id"));
            }
            return user;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public Team getTeam(String name) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(GET_TEAM_BY_NAME)) {
            statement.setString(1, name);
            ResultSet res = statement.executeQuery();
            Team team = Team.createTeam(name);
            if (res.next()) {
                team.setId(res.getInt("id"));
            }
            return team;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(GET_ALL_TEAMS)) {
            ResultSet res = statement.executeQuery();
            List<Team> teams = new ArrayList<>();
            while (res.next()) {
                Team newTeam = Team.createTeam(res.getString("name"));
                newTeam.setId(res.getInt("id"));
                teams.add(newTeam);
            }
            return teams;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, team.getName());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
                return true;
            } else
                return false;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = connect();
        try (PreparedStatement insert = connection.prepareStatement(SET_TEAM_FOR_USER)) {
            connection.setAutoCommit(false);
            insert.setInt(1, user.getId());
            for (Team team : teams) {
                insert.setInt(2, team.getId());
                insert.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
                throw new DBException("Rollback failed", e);
            }
            throw new DBException("Commit failed", e);
        } finally {
            closeConnection(connection);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(GET_USER_TEAM)) {
            List<Team> teams = new ArrayList<>();
            statement.setInt(1, user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Team newTeam = Team.createTeam(resultSet.getString("name"));
                newTeam.setId(resultSet.getInt("id"));
                teams.add(newTeam);
            }
            return teams;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(DELETE_TEAM)) {
            statement.setInt(1, team.getId());
            statement.setString(2, team.getName());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (PreparedStatement statement = connect().prepareStatement(UPDATE_TEAM)) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
			e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    private Connection connect() throws DBException {
        try (FileReader reader = new FileReader("app.properties")) {
            Properties p = new Properties();
            p.load(reader);
            return DriverManager.getConnection(p.getProperty("connection.url"));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DBException("Connection failed", ex);
        }
    }

    private void closeConnection(Connection con) throws DBException{
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

}
